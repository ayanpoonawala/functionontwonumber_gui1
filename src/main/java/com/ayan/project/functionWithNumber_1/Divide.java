package com.ayan.project.functionWithNumber_1;

public class Divide {
	long leftValue,rightValue,result;

	public Divide(long leftValue, long rightValue) {
		super();
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}
	
	public long value() {
		try {
		result = leftValue/rightValue;
		}catch(Exception e) {
			System.out.println("Error : "+ e.getMessage());
		}
		
		return result;
		
	}

}
