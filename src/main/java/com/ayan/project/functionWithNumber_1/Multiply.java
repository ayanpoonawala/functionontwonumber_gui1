package com.ayan.project.functionWithNumber_1;

public class Multiply {
	long leftVal;
	long rightVal;
	long result;
	public Multiply(long leftVal, long rightVal) {
		super();
		this.leftVal = leftVal;
		this.rightVal = rightVal;
	}
	
	public long result() {
		result = leftVal*rightVal;
		return result;
	}
	
	


}
